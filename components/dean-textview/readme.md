### 版本发布
##### 父组件引入
``` html
<template>
	<deanTextview v-model="value" class="input-area"></deanTextview>
</template>
<style lang="scss">
.input-area /deep/ .input-area { //穿透到子组件样式
	width: 100vw;
	height: 25vw;
	background-color: #0081ff;
}
</style>
```
### 0.0.2 版本更新
解决文本域换行问题
### 0.0.3 版本更新
添加placeholder支持
``` html
<deanTextview v-model="value" class="input-area" placeholder="请输入文本"></deanTextview>
```
### 0.0.4 版本更新
添加disable支持
修复小程序端Dom支持
``` html
<deanTextview v-model="value" class="input-area" placeholder="请输入文本" disable></deanTextview>
```