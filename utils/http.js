import {
	getToken
} from './token.js'
const BASE_URL = "http://zs.91gongju.cn:10003"
// const BASE_URL = "http://localhost:10003"

export function request({
	url,
	token = true,
	data = {},
	method = "GET"
}) {
	let param = {
		url,
		token,
		data,
		method
	}
	uni.showLoading({
		title: "加载中"
	});
	return new Promise((resolve, reject) => {
		uni.request({
			url: `${BASE_URL}${param.url}`,
			data: param.data,
			method: param.method,
			header: {
				"Authorization": ((param.token) ? ("Bearer " + getToken()) : '')
			},
			complete() {
				uni.hideLoading();
			},
			fail(error) {
				reject(error);
			},
			success(response) {
				let data = response.data;
				if (data.code === 401) {
					console.log("失败401", data)
					uni.removeStorage({
						key: "Admin-Token",
						complete() {
							uni.navigateTo({
								url: "/pages/homechild/login/login"
							})
						}
					})
				}
				if (data.code == 500) {
					console.log("失败500", data)
					uni.showToast({
						title: data.msg,
						icon: "none",
					})
				} else if (data.code !== 200) {
					console.log("失败非200", data)
					uni.showToast({
						title: data.msg,
						icon: "none",
					})
				}
				resolve(response);
			}
		})
	})
}
