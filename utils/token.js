export function getToken() {
	let TOKEN = "";
	uni.getStorage({
		key: "Admin-Token",
		success(data) {
			TOKEN = data.data;
		},fail(error) {
			console.log(error);
		}
	})
	return TOKEN;
}
